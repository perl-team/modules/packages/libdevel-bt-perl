libdevel-bt-perl (0.06-5) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * Simplify BTS URL.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:47:20 +0100

libdevel-bt-perl (0.06-4) unstable; urgency=medium

  * Drop override_dh_auto_test, debhelper runs tests verbosely since
    9.20150501.
  * Add build and runtime dependency on perl-debug. (Closes: #808480)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Dec 2015 15:40:10 +0100

libdevel-bt-perl (0.06-3) unstable; urgency=medium

  * Team upload.
  * Add a patch fixing testsuite failures on slow hosts (particularly mips).
    (See #721421)
  * Mark the package as autopkgtestable.

 -- Niko Tyni <ntyni@debian.org>  Sat, 27 Sep 2014 11:11:32 +0300

libdevel-bt-perl (0.06-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.

  [ Daniel Lintott ]
  * Add patch to fix FTBFS on hurd (Closes: #721420)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add patch to improve test situation on Hurd. Cf. #721421
    Thanks to Leon Timmermans for the patch.
  * Make tests verbose for easier debugging.
  * Add /me to Uploaders.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Thu, 04 Sep 2014 14:23:43 +0200

libdevel-bt-perl (0.06-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Don't install README anymore.
  * debian/copyright: update to Copyright-Format 1.0.
  * Update year of upstream copyright.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Use debhelper 9 for hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Mar 2012 15:24:22 +0100

libdevel-bt-perl (0.05-1) unstable; urgency=low

  * Initial Release. (Closes: #596872)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Wed, 15 Sep 2010 11:56:39 +0100
